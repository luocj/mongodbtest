package com.mng.utils.sequence;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @GeneratedValue for SaveMongoEventListener update id(_id) with increase on Document Sequences.sequenceId
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface GeneratedValue
{
/*  For Auto generate _id, the id data type must be as fllows:
    classes.add(ObjectId.class);
    classes.add(String.class);
    classes.add(BigInteger.class);*/
}
